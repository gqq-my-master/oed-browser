﻿using System;
using System.IO;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;


namespace oed
{
	class Program
	{
		static void Main(string[] args)
		{
			new DriverManager().SetUpDriver(new FirefoxConfig());

			var options = new FirefoxOptions();
			options.AddArguments("-profile", @"B:\cookies");
			var driver = new FirefoxDriver(@"B:\", options);

			driver.Navigate().GoToUrl("https://www.oed.com/");
			Console.ReadKey();


			Collect(driver);

			Console.WriteLine("done");

		}

		private static void Collect(FirefoxDriver driver)
		{
			driver.Navigate().GoToUrl(@"https://www.oed.com/search?browseType=sortFrequency&case-insensitive=true&nearDistance=1&obsolete=false&ordered=false&page=1&pageSize=100&regionClass=North+America&scope=ENTRY&sort=frequency&type=dictionarysearch&usageClass=colloquial+and+slang");

			using (var sw = new StreamWriter(@"B:\words.txt"))
			{
				for (; ; )
				{
					var words = driver.FindElements(By.ClassName("word"));
					foreach (var w in words)
					{
						var frequencyBand = w.FindElement(By.XPath("./../*[contains(@class, 'frequencyBand')]/span"));
						int frequency = int.Parse(frequencyBand.GetAttribute("rel"));
						if (frequency >= 4)
							sw.WriteLine(w.Text);
						else
							return;
					}

					var next = driver.FindElement(By.CssSelector(".pagination .next a"));
					if (next == null)
						break;
					next.Click();
				}
			}

		}
	}
}
